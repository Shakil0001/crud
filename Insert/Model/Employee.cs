﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insert.Model
{
    public class Employee
    {
        public int EmployeeID { get; set; }
        public string Name { get; set; }
        public string Designation { get; set; }
        public string ContactNO { get; set; }
        public int DepartmentID { get; set; }

        public Employee(string name, string designation, string contactNo, int departmentID)
        {
            Name = name;
            Designation = designation;
            ContactNO = contactNo;
            DepartmentID = departmentID;
        }
        public Employee()
        {

        }

    }
}