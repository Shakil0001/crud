﻿using Insert.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Insert.DAL
{
    public class DepartmentGateway
    {
        string connectionstring =
           WebConfigurationManager.ConnectionStrings["ComapnyUIConnectionStrings"].ConnectionString;
        public List<Department> GetAllDepartments()
        {
            SqlConnection connection = new SqlConnection(connectionstring);
            string query = "SELECT * FROM DEPARTMENTS";
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<Department> departments = new List<Department>();
            while (reader.Read())
            {
                Department department = new Department();
                department.DepartmentID = (int)reader["DepartmentID"];
                department.Code = reader["Code"].ToString();
                department.Name = reader["Name"].ToString();
                departments.Add(department);
            }
            reader.Close();
            connection.Close();
            return departments;
        }
        public Department Getdepartmentbyid(int id)
        {
            SqlConnection connection = new SqlConnection(connectionstring);
            string query = "SELECT * FROM DEPARTMENTS WHERE DEPARTMENTID="+id;
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            Department department = new Department();
            while (reader.Read())
            {
                department.DepartmentID = (int)reader["DepartmentID"];
                department.Code = reader["Code"].ToString();
                department.Name = reader["Name"].ToString();
                
            }
            reader.Close();
            connection.Close();
            return department;
        }
    }
}