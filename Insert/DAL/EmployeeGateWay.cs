﻿using Insert.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Insert.DAL
{
    public class EmployeeGateWay
    {
        string connectionstring =
            WebConfigurationManager.ConnectionStrings["ComapnyUIConnectionStrings"].ConnectionString;
        public int SaveEmployee(Employee employee)
        {
            SqlConnection connection = new SqlConnection(connectionstring);
            string query = "INSERT INTO EMPLOYEE VALUES('" + employee.Name + "','" + employee.Designation + "','" + employee.ContactNO + "','" + employee.DepartmentID + "')";
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            connection.Close();
            return rowAffected;

        }
        public List<Employee> GetAllEmployee()
        {

            SqlConnection connection = new SqlConnection(connectionstring);
            string query = "SELECT * FROM EMPLOYEE";
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            List<Employee> employees = new List<Employee>();
            while (reader.Read())
            {
                Employee employee = new Employee();
                employee.EmployeeID = (int)reader["EmployeeID"];
                employee.Name = reader["Name"].ToString();
                employee.ContactNO = reader["ContactNO"].ToString();
                employee.Designation = reader["Designation"].ToString();
                employee.DepartmentID = (int)reader["DepartmentID"];
                employees.Add(employee);
            }
            reader.Close();
            connection.Close();
            return employees;
        }
        public Employee ISExit(Employee employee)
        {
            SqlConnection connection = new SqlConnection(connectionstring);
            string query = "SELECT * FROM EMPLOYEE WHERE CONTACTNO=" + employee.ContactNO;
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            Employee semployee = new Employee();
            if (reader.HasRows)
            {
                reader.Read();
                semployee.EmployeeID = (int)reader["EmployeeID"];
                semployee.Name = reader["Name"].ToString();
                semployee.ContactNO = reader["ContactNO"].ToString();
                semployee.Designation = reader["Designation"].ToString();

            }
            reader.Close();
            connection.Close();
            return semployee;
        }
        public bool UpdateEmployee (Employee employee)
        {
            string connectionstring =
            WebConfigurationManager.ConnectionStrings["ComapnyUIConnectionStrings"].ConnectionString;
            SqlConnection connection = new SqlConnection(connectionstring);
            string query = "UPDATE EMPLOYEE SET '"+employee.Name+"','"+employee.Designation+"' where EmployeeID='"+employee.EmployeeID+"' ";
            SqlCommand command = new SqlCommand(query, connection);
            connection.Open();
            int rowAffected = command.ExecuteNonQuery();
            connection.Close();

            if (rowAffected > 0)
            {
                return true;

            }
            return false;
        }
      
    }
}