﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UpdateEmployee.aspx.cs" Inherits="Insert.UI.UpdateEmployee" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                 <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="ContactNO"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="ContactNOtxtbox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Nametxtbox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Designation"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Designationtxtbox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="upadtebtn" runat="server" Text="Update" OnClick="upadtebtn_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:Label ID="messagelabel" runat="server"></asp:Label>
    </form>
</body>
</html>
