﻿using Insert.BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insert.UI
{
    public partial class ShowAll : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            EmployeeManager employeemanager = new EmployeeManager();
            ShowAllGridview.DataSource = employeemanager.GetAllEmployee();
            ShowAllGridview.DataBind();
        }
    }
}