﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="Insert.Index" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Name"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Nametxtbox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="Designation"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="Designationtxtbox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="ContactNO"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="ContactNOtxtbox" runat="server"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="Department"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="Departmentdropdownlist" runat="server" OnSelectedIndexChanged="Departmentdropdownlist_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
                    </td>
                </tr>
            <tr>
                <td></td>
                <td>
                    <asp:Label ID="departmentLabel" runat="server" Text=""></asp:Label>
                </td>
            </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="insertbtn" runat="server" Text="Insert" OnClick="insertbtn_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:Label ID="messagelabel" runat="server" Text="Label"></asp:Label>
    </form>
</body>
</html>
