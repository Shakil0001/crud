﻿using Insert.BLL;
using Insert.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insert.UI
{
    public partial class UpdateEmployee : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        EmployeeManager employeemanager = new EmployeeManager();
        protected void upadtebtn_Click(object sender, EventArgs e)
        {
            Employee employee = new Employee();
            employee.ContactNO = ContactNOtxtbox.Text;
            employee.Name = Nametxtbox.Text;
            employee.Designation = Designationtxtbox.Text;
            messagelabel.Text = employeemanager.UpdateEmployee(employee);
        }
    }
}