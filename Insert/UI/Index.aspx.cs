﻿using Insert.BLL;
using Insert.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insert
{
    public partial class Index : System.Web.UI.Page
    {
        EmployeeManager employeemanager = new EmployeeManager();
        DepartmentManager departmentmanager = new DepartmentManager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
            Departmentdropdownlist.DataTextField = "Code";
            Departmentdropdownlist.DataValueField = "DepartmentID";
            Departmentdropdownlist.DataSource = departmentmanager.GetAllDepartments();
            Departmentdropdownlist.DataBind();
            }
           
        }

        protected void insertbtn_Click(object sender, EventArgs e)
        {
            string name = Nametxtbox.Text;
            string designation = Designationtxtbox.Text;
            string contactNo = ContactNOtxtbox.Text;
            int departmentID = Convert.ToInt32(Departmentdropdownlist.SelectedValue);
            Employee employee = new Employee( name,  designation, contactNo,departmentID);
            messagelabel.Text = employeemanager.SaveEmployee(employee);
        }

        protected void Departmentdropdownlist_SelectedIndexChanged(object sender, EventArgs e)
        {
            int departmentID = Convert.ToInt32(Departmentdropdownlist.SelectedValue);
            Department department = departmentmanager.Getdepartmentbyid(departmentID);
            departmentLabel.Text = department.Name;
        }
    }
}