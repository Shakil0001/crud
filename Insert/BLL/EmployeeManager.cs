﻿using Insert.DAL;
using Insert.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insert.BLL
{
    public class EmployeeManager
    {
        EmployeeGateWay employeegateway = new EmployeeGateWay();
        public string SaveEmployee(Employee employee)
        {
           
            if (employee.ContactNO.Length >= 7)
            {
                Employee semployee = employeegateway.ISExit(employee);
                if (semployee == null)
                {
                    int rowAffected = employeegateway.SaveEmployee(employee);

                    if (rowAffected > 0)
                    {
                        return "Insert Successfully";
                    }
                    else
                    {
                        return "Insert Failed";
                    }
                }
                else
                {
                    return "ContactNo must be unique";
                }
                
            }
            else
            {
                return "Contact No must be at least Seven Digits.";
            }  
        }
        public List<Employee> GetAllEmployee()
        {
           return employeegateway.GetAllEmployee();
        }

        public string UpdateEmployee(Employee employee)
        {
            Employee semployee = employeegateway.ISExit(employee);
            if (semployee == null)
            {
                return "ContactNo does not exists.";
            }
            else
            {
                employee.EmployeeID = employee.EmployeeID;
                if (employeegateway.UpdateEmployee(employee))
                {
                    return "update Successfully";
                }
                return "update failed";

            }
                
        }
    }
}