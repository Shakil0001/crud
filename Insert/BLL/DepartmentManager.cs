﻿using Insert.DAL;
using Insert.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Insert.BLL
{
    public class DepartmentManager
    {
        DepartmentGateway departmentgateway = new DepartmentGateway();
        public List<Department> GetAllDepartments()
        {
            return departmentgateway.GetAllDepartments();
        }
        public Department Getdepartmentbyid(int id)
        {
            return departmentgateway.Getdepartmentbyid(id);
        }
    }
}